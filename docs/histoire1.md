# Une loutre à la plage

Il était une fois une loutre
violette qui voulait aller
jouer au ballon à la plage
mais elle n'avait pas d'amis
ni de ballon d'ailleurs.

Elle décida d'aller voir
l'éléphant bleu son voisin.

## Contributeurs (dans l'ordre alphabétique)

* Fred
* Glenn R.
* Jérémy O
* Micka Petit
* Olivia Surget (+ graphiquement)
* Paul
* Raba M
* Rudy
* Rudy I.
* Stephane C.
